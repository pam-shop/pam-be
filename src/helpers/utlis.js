/* eslint-disable no-useless-escape */
const removeUnicode = (str) => {
  let newStr = str;
  if (newStr) {
    newStr = newStr.toLowerCase();
    newStr = newStr.replace(/̀|̣.|΄|̃|̉/g, '');
    newStr = newStr.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    newStr = newStr.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    newStr = newStr.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    newStr = newStr.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    newStr = newStr.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    newStr = newStr.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    newStr = newStr.replace(/đ/g, 'd');
    newStr = newStr.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,
      '-'
    );

    newStr = newStr.replace(/[^a-z0-9\-]/g, '');
    // thay thế 2- thành 1-
    newStr = newStr.replace(/-+-/g, '-');
    newStr = newStr.replace(/^\-+|\-+$/g, '');

    return newStr;
  }
  return '';
};

const nameToUrl = (name) => removeUnicode(name);

const queryBuilder = (params) => {
  const arrayParam = [];
  // eslint-disable-next-line array-callback-return
  Object.keys(params).map((key) => {
    if (params[key]) {
      arrayParam.push(key + '=' + params[key]);
    }
  });
  return arrayParam.length > 0 ? '?' + arrayParam.join('&') : '';
};

const FALSY_VALUE_DENY = ['undefined', 'null', ''];
// const VALID_KEY = ['username'];
const validateQueryValue = (key, value) => !FALSY_VALUE_DENY.includes(value);

const formatQuery = (object) => {
  let result = {};
  Object.keys(object).forEach((key) => {
    const value = object[key];
    const isValueValid = validateQueryValue(key, value);
    if (isValueValid) {
      result = {
        ...result,
        [key]: value,
      };
    }
  });
  return result;
};

export { nameToUrl, queryBuilder, formatQuery };
