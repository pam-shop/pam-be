import express from 'express';
import mongoose from 'mongoose';
import path from 'path';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import config from '~/config';
import authRouter from '~/router/authRouter';
import categoryRouter from '~/router/categoryRouter.js';
import subCategoryRouter from '~/router/subCategoryRouter';
import productRouter from '~/router/productRouter';

mongoose.connect(config.databaseUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const app = express();
const port = process.env.PORT || 1111;

app.use(cors());
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morgan('tiny'));

app.get('/', (req, res) => {
  res.json({
    message: 'PAM SHOP CHECK',
    env: config.env,
  });
});

app.use('/api/auth', authRouter);
app.use('/api/category', categoryRouter);
app.use('/api/subcategory', subCategoryRouter);
app.use('/api/product', productRouter);

app.listen(port, () => console.log(`Server listening on port ${port}!`));
