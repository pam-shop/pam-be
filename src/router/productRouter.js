import express from 'express';
import productController from '~/controller/productController';
// import { uploadMultiple } from '~/middleware/imageMiddleware';

const router = express.Router();

router.get('/:productId', productController.getOne);
router.get('/', productController.getProduct);
router.post('/', productController.createProduct);
router.put('/:productId', productController.editProduct);
router.delete('/:productId', productController.deleteProduct);

export default router;
