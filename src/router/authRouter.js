import express from 'express';
import authControler from '~/controller/authController';

const router = express.Router();

router.post('/login', authControler.login);
router.post('/register', authControler.register);

export default router;
