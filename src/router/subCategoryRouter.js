import express from 'express';
import subCategoryController from '~/controller/subCategoryController';

const router = express.Router();

router.get('/', subCategoryController.getSubcategory);
router.get('/:subCategoryId', subCategoryController.getOne);
router.post('/', subCategoryController.createSubcategory);
router.put('/:subCategoryId', subCategoryController.editSubcategory);
router.delete('/:subCategoryId', subCategoryController.deleteSubcategory);

export default router;
