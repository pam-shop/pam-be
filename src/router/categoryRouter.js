import express from 'express';
import categoryController from '~/controller/categoryController';

const router = express.Router();

router.get('/:categoryId', categoryController.getOne);
router.get('/', categoryController.getCategory);
router.post('/', categoryController.createCategory);
router.put('/:categoryId', categoryController.editCategory);
router.delete('/:categoryId', categoryController.deleteCategory);

export default router;
