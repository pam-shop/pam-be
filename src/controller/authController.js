import md5 from 'md5';
import UserModel from '~/model/authModel';
import { generateToken } from '~/helpers/privateToken';

const loginController = async (req, res) => {
  const { username, password } = req.body;
  const findUser = {
    username,
    password: md5(password),
  };

  const user = await UserModel.findOne(findUser);
  if (!user) {
    return res.json({
      message: 'Login fail',
    });
  }

  const tokenDemo = generateToken(findUser);
  res.cookie('tokenDemo', tokenDemo);

  return res.json({
    message: 'Login success',
    privateToken: tokenDemo,
  });
};

const registerController = async (req, res) => {
  const { username, password } = req.body;
  const oldUser = await UserModel.findOne({ username });
  if (oldUser) {
    if (!oldUser.isActive) {
      return res.status(404).json({
        message: 'Tài khoản đã bị khoá',
      });
    }
    return res.status(404).json({
      message: 'Tài khoản đã tồn tại',
    });
  }

  const obj = {
    username,
    password: md5(password),
  };

  const newUser = new UserModel(obj);
  newUser.save();

  return res.json({
    message: 'Create Success',
  });
};

export default {
  login: loginController,
  register: registerController,
};
