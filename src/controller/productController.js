import ProductModel from '~/model/productModel';
import { formatQuery, nameToUrl } from '~/helpers/utlis';

const getOne = async (req, res) => {
  const { productId } = req.params;
  try {
    const product = await ProductModel.findOne({ _id: productId });
    res.json(product);
  } catch (error) {
    res.json({
      message: 'get product',
      error,
    });
  }
};

const editProduct = (req, res) => {
  res.send({});
};

const createProduct = (req, res) => {
  try {
    const { name, description, price, categoryId, subCategoryId, inventory, files } = req.body;
    console.log('files', files);
    const objProduct = {
      name,
      slug: nameToUrl(name),
      description,
      price,
      categoryId,
      subCategoryId,
      inventory,
    };
    // const newProduct = new ProductModel(objProduct);
    res.send({
      message: 'ok',
      product: objProduct,
    });
  } catch (error) {
    res.send({
      message: 'create error',
      error,
    });
  }
};

const deleteProduct = async (req, res) => {
  try {
    const { productId } = req.params;

    await ProductModel.findByIdAndDelete({
      _id: productId,
    });

    res.json({
      message: 'Delete success',
    });
  } catch (error) {
    res.send({
      message: 'delete error',
      error,
    });
  }
};

const getProduct = async (req, res) => {
  try {
    const { page = 1, limit = 10, ...rest } = req.query;
    const skip = parseInt((page - 1) * limit, 10);
    const result = formatQuery(rest);
    const products = await ProductModel.find(result).skip(skip).limit(parseInt(limit, 10));
    res.json({
      data: products,
      total: products.length,
    });
  } catch (error) {
    res.send({
      message: 'get product',
      error,
    });
  }
};

export default { getOne, getProduct, deleteProduct, createProduct, editProduct };
