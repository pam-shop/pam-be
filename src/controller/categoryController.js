import CategoryModel from '~/model/categoryModel';
import { nameToUrl } from '~/helpers/utlis';

const getCategory = async (req, res) => {
  try {
    // join collection
    const category = await CategoryModel.aggregate([
      {
        $lookup: {
          from: 'subcategories',
          localField: 'categoryId',
          foreignField: 'categoryId',
          as: 'subCategory',
        },
      },
    ]);

    return res.json({
      category,
      total: category.length,
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'category index',
      error,
    });
  }
};

const getOne = async (req, res) => {
  try {
    const { categoryId } = req.params;
    const category = await CategoryModel.aggregate([
      {
        $lookup: {
          from: 'subcategories',
          localField: 'categoryId',
          foreignField: 'categoryId',
          as: 'subCategory',
        },
      },
    ]);
    // eslint-disable-next-line eqeqeq
    return res.json(category.filter((item) => item._id == categoryId)[0]);
  } catch (error) {
    return res.status(500).send({
      ctl: 'category index',
      error,
    });
  }
};

const createCategory = async (req, res) => {
  try {
    const { body } = req;
    if (!body || !body.name) {
      return res.status(500).send({
        ctl: 'Category create',
        message: 'Invalte body',
      });
    }

    const { name, order, categoryId } = req.body;
    const oldCategory = await CategoryModel.findOne({ name });
    if (oldCategory) {
      return res.send({
        ctl: 'Category create',
        message: 'Category is already exist',
      });
    }

    const obj = {
      name,
      order,
      categoryId,
      slug: nameToUrl(name),
    };

    const nameCategory = new CategoryModel(obj);
    nameCategory.save();

    return res.send({
      message: 'Create category success',
      data: nameCategory,
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'Category create',
      error,
    });
  }
};

const deleteCategory = async (req, res) => {
  const { categoryId } = req.params;

  await CategoryModel.findByIdAndDelete({
    _id: categoryId,
  }).catch((err) =>
    res.json({
      message: 'Delete fail',
      error: err,
    })
  );

  return res.json({
    message: 'Delete fuccess',
  });
};

const editCategory = async (req, res) => {
  try {
    const { categoryId } = req.params;
    const { name = null, order = null, isActive = null, categoryId: cgId = null } = req.body;
    const objEdit = {
      name,
      slug: nameToUrl(name),
      order,
      isActive,
      categoryId: cgId,
      updatedDate: Date.now(),
    };
    await CategoryModel.findByIdAndUpdate(
      {
        _id: categoryId,
      },
      objEdit
    );
    return res.json({
      message: 'Update Success',
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'category edit',
      error,
    });
  }
};

export default {
  getCategory,
  editCategory,
  createCategory,
  deleteCategory,
  getOne,
};
