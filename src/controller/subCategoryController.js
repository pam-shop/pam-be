import SubCategory from '~/model/subCategoryModel';
import { nameToUrl } from '~/helpers/utlis';

const getSubcategory = async (req, res) => {
  try {
    const subCategory = await SubCategory.find();
    return res.json({
      data: subCategory,
      total: subCategory.length,
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'Subcategory index',
      error,
    });
  }
};

const getOne = async (req, res) => {
  try {
    const { subCategoryId } = req.params;
    const subCategory = await SubCategory.findOne({ _id: subCategoryId });
    return res.json(subCategory);
  } catch (error) {
    return res.status(500).send({
      ctl: 'category index',
      error,
    });
  }
};

const createSubcategory = async (req, res) => {
  try {
    const { body } = req;
    if (!body) {
      return res.status(500).send({
        ctl: 'Subcategory create',
        message: 'Invalte body',
      });
    }

    const { name, categoryId, order } = req.body;
    const oldSubcategory = await SubCategory.findOne({ name });
    if (oldSubcategory) {
      return res.send({
        ctl: 'Subcategory create',
        message: 'Subcategory is already exist',
      });
    }

    const obj = {
      id: categoryId,
      name,
      categoryId,
      slug: nameToUrl(name),
      order,
    };

    const newSubcategory = new SubCategory(obj);
    newSubcategory.save();

    return res.send({
      message: 'create subcategory success',
      newSubcategory,
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'Category create',
      error,
    });
  }
};

const deleteSubcategory = async (req, res) => {
  const { subCategoryId } = req.params;

  await SubCategory.findByIdAndDelete({
    _id: subCategoryId,
  }).catch((err) => {
    res.json({
      message: 'Delete fail',
      error: err,
    });
  });

  res.json({
    message: 'Delete fuccess',
  });
};

const editSubcategory = async (req, res) => {
  try {
    const { subCategoryId } = req.params;
    const { categoryId } = req.body;
    const objEdit = {
      categoryId,
    };
    await SubCategory.findByIdAndUpdate(
      {
        _id: subCategoryId,
      },
      objEdit
    );
    return res.json({
      message: 'Update Success',
    });
  } catch (error) {
    return res.status(500).send({
      ctl: 'category edit',
      error,
    });
  }
};

export default {
  getSubcategory,
  editSubcategory,
  createSubcategory,
  deleteSubcategory,
  getOne,
};
