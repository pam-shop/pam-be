import multer from 'multer';

const storage = multer.diskStorage({
  destination: (_req, _file, callback) => callback(null, `${__dirname}/public/images`),
  filename: (_req, file, callback) => {
    const math = ['image/png', 'image/jpeg'];
    if (math.indexOf(file.mimetype) === -1) {
      const errorMess = `File ${file.originalname} invalid.`;
      return callback(errorMess, null);
    }
    const filename = `${Date.now()}-${file.originalname}`;
    return callback(null, filename);
  },
});

const upload = multer({ storage });

const uploadSingle = (name) => upload.single(name);

// const handleUploadMultiple = async (name, limit = 5) => {
//   // console.log(name);
//   // console.log(limit);
//   // console.log(p1);
//   // console.log(p2);
// };

// const uploadMultiple = async (req, res, next) => {
//   handleUploadMultiple('images', 5)(req, res);
//   next();
// };

export { uploadSingle };
