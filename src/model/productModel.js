import mongoose from 'mongoose';

const productModel = new mongoose.Schema({
  name: { type: String, required: true },
  categoryId: { type: Number, default: 1 },
  subCategoryId: { type: Number, default: 1 },
  description: String,
  thumnbail: String,
  images: { type: Array, default: [] },
  slug: String,
  price: Number,
  inventory: { type: Number, default: 5 },
  createdDate: { type: Number, default: Date.now() },
  updatedDate: { type: Number, default: Date.now() },
});

export default mongoose.model('Product', productModel);
