import mongoose from 'mongoose';

const userModel = new mongoose.Schema({
  username: String,
  password: String,
  isActive: { type: Boolean, default: true },
});

export default mongoose.model('User', userModel);
