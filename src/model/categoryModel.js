import mongoose from 'mongoose';

const categoryModel = new mongoose.Schema({
  name: String,
  categoryId: Number,
  slug: String,
  order: { type: Number, default: 0 },
  isActive: { type: Boolean, default: true },
  createdDate: { type: Number, default: Date.now() },
  updatedDate: { type: Number, default: Date.now() },
});

export default mongoose.model('Category', categoryModel);
